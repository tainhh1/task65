package com.devcamp.task65.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task65.model.Customer;

public interface ICustomerRepo extends JpaRepository<Customer, Long> {
  Customer findByCustomerId(Long customerId);
}
