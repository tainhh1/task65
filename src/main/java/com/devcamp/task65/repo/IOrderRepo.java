package com.devcamp.task65.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task65.model.Order;

public interface IOrderRepo extends JpaRepository<Order, Long> {
  Order findByOrderId(Long paramOrderId);
}
