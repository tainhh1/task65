package com.devcamp.task65.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task65.model.Product;

public interface IProductRepo extends JpaRepository<Product, Long> {

}
