package com.devcamp.task65.controller;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.task65.model.Customer;
import com.devcamp.task65.model.Order;
import com.devcamp.task65.model.Product;
import com.devcamp.task65.repo.ICustomerRepo;
import com.devcamp.task65.repo.IOrderRepo;
import com.devcamp.task65.repo.IProductRepo;

@CrossOrigin
@RestController
public class Controller {
  @Autowired
  ICustomerRepo pCustomerRepo;
  @Autowired
  IOrderRepo pIOrderRepo;
  @Autowired
  IProductRepo productRepo;
  
  @GetMapping ("/devcamp-orders")
  public ResponseEntity<Set<Order>> getOrderByCustomerId(@RequestParam (value = "userId") Long userId) {
    try {
      
      Customer customerFound = pCustomerRepo.findByCustomerId(userId);
      
      if (customerFound != null) {
        return new ResponseEntity<>(customerFound.getOrders() ,HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
      
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @GetMapping("/devcamp-products")
  public ResponseEntity<Set<Product>> getProductByOrderId(@RequestParam(name = "orderId") Long orderId) {
    try {
      
      Order orderFound = pIOrderRepo.findByOrderId(orderId);
      
      if (orderFound != null) {
        return new ResponseEntity<>(orderFound.getProducts(), HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
      
    } catch (Exception e) {
      
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      
    }
  }
}
