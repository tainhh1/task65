package com.devcamp.task65;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task65Application {

	public static void main(String[] args) {
		SpringApplication.run(Task65Application.class, args);
	}

}
