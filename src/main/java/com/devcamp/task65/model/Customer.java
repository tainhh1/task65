package com.devcamp.task65.model;

import java.util.Set;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "customers")
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long customerId;
  @Column(name = "fullName")
  private String fullname;
  @Column(name = "email")
  private String email;
  @Column(name = "phone")
  private String phone;
  @Column(name = "address")
  private String address;
  
  @OneToMany(mappedBy = "customers", cascade = CascadeType.ALL)
  @JsonManagedReference
  private Set<Order> orders;

  public Customer() {
    super();
    // TODO Auto-generated constructor stub
  }

  public Customer(Long customerId, String fullname, String email, String phone, String address) {
    super();
    this.customerId = customerId;
    this.fullname = fullname;
    this.email = email;
    this.phone = phone;
    this.address = address;
  }

  public long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Set<Order> getOrders() {
    return orders;
  }

  public void setOrders(Set<Order> orders) {
    this.orders = orders;
  }

}
