package com.devcamp.task65.model;

import java.util.Set;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long orderId;
  @Column(name = "orderCode", unique = true)
  private String orderCode;
  @Column(name = "pizzaSize")
  private String pizzaSize;
  @Column(name = "pizzaType")
  private String pizzaType;
  @Column(name = "voucherCode")
  private String voucherCode;
  @Column(name = "price")
  private Long price;
  @Column(name = "paid")
  private Long paid;

  @ManyToOne
  @JoinColumn(name = "customerId")
  @JsonIgnore
  private Customer customers;

  @JsonManagedReference
  @OneToMany(mappedBy = "orders", cascade = CascadeType.ALL)
  private Set<Product> products;

  public Order() {
    super();
    // TODO Auto-generated constructor stub
  }

  public Order(Long orderId, String orderCode, String pizzaSize, String pizzaType,
      String voucherCode, Long price, Long paid, Customer customers) {
    super();
    this.orderId = orderId;
    this.orderCode = orderCode;
    this.pizzaSize = pizzaSize;
    this.pizzaType = pizzaType;
    this.voucherCode = voucherCode;
    this.price = price;
    this.paid = paid;
    this.customers = customers;
  }

  public long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getOrderCode() {
    return orderCode;
  }

  public void setOrderCode(String orderCode) {
    this.orderCode = orderCode;
  }

  public String getPizzaSize() {
    return pizzaSize;
  }

  public void setPizzaSize(String pizzaSize) {
    this.pizzaSize = pizzaSize;
  }

  public String getPizzaType() {
    return pizzaType;
  }

  public void setPizzaType(String pizzaType) {
    this.pizzaType = pizzaType;
  }

  public String getVoucherCode() {
    return voucherCode;
  }

  public void setVoucherCode(String voucherCode) {
    this.voucherCode = voucherCode;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public Long getPaid() {
    return paid;
  }

  public void setPaid(Long paid) {
    this.paid = paid;
  }

  public Customer getCustomers() {
    return customers;
  }

  public void setCustomers(Customer customers) {
    this.customers = customers;
  }

  public Set<Product> getProducts() {
    return products;
  }

  public void setProducts(Set<Product> products) {
    this.products = products;
  }

}
