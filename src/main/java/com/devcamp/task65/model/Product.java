package com.devcamp.task65.model;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long productId;
  @Column(name = "sname")
  private String name;
  @Column(name = "type")
  private String type;
  @Column(name = "color")
  private String color;
  @Column(name = "price")
  private String price;
  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "orderId")
  private Order orders;

  public Product() {
    super();
    // TODO Auto-generated constructor stub
  }

  public Product(Long productId, String name, String type, String color, String price,
      Order orders) {
    super();
    this.productId = productId;
    this.name = name;
    this.type = type;
    this.color = color;
    this.price = price;
    this.orders = orders;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public Order getOrders() {
    return orders;
  }

  public void setOrders(Order orders) {
    this.orders = orders;
  }

}
